L’auto-hébergement
==================

C’est quoi l’auto-hébergement?
------------------------------

La plupart des sites web que vous avez l’habitude de consulter (vos
courriels, les réseaux sociaux…) sont hébergés sur des serveurs, quelque
part dans le monde. C’est quoi un serveur ? C’est ni plus ni moins un
ordinateur.

Le principe est simple : votre navigateur ne fait qu’échanger des
données avec ces serveurs pour que vous puissiez utiliser les services
qu’ils proposent.

<div class="alert alert-info">
    <p>Lorsque vous voulez consulter vos e-mails, votre navigateur va
chercher sur un serveur quelque part dans le monde tous vos messages,
qui sont alors téléchargés vers votre ordinateur.</p>

    <p>C'est comme si pour lire votre courrier postal, vous deviez aller à la
poste, (<em>attendre dans la file d'attente</em>,) demander au facteur :
    <blockquote>T'as du courrier pour moi ?</blockquote>
(<em>Attendre qu'il aille chercher les colis</em>), les récupérer puis enfin lire votre courrier. C'est le bureau de poste qui l'avait,
jusqu'à ce que vous le releviez.</p>
</div>

Ce document décrit donc comment installer chez soi les services que vous
utilisez habituellement chez les “grands noms” du net en échange de
publicités et de vos données personnelles.

*Notez bien que ce que l’on propose de faire sur une machine à la maison
peut aussi être réalisé sur un serveur mutualisé.* Ce n’est pas
exactement de l’auto-hébergement, mais cela participe à la
décentralisation du web.

Comment ça marche?
------------------

En définitive, si vous choisissez l’auto-hébergement, votre utilisation
des services web ne sera pas si différente, à ceci près que vos données
resteront chez vous.

Voilà ce qu’il va se passer : un ordinateur sera relié à votre modem
(aussi appelé *machin-box*) et donc connecté à internet. Ce sera votre
*serveur*. Notez que cet ordinateur sera soit du matériel de
récupération ou bien une carte qui consomme peu d’électricité[^1].

Lorsque vous irez consulter votre courrier électronique, votre
ordinateur personnel ira en fait demander ces messages sur votre
serveur. Quand quelqu’un voudra consulter votre site web, il sera dirigé
vers votre serveur dans votre maison. Pour que cela soit possible, il
faudra indiquer le chemin pour accéder à votre serveur. Imaginez ça
comme un itinéraire.

<div class="alert alert-info">
<p>Autrement dit, votre serveur aura une adresse, puis des mécanismes
appelés <abbr>DNS</abbr> se chargeront d’y conduire les visiteurs. Cette
adresse, c’est en fait celle de votre connexion internet (l'adresse de la
machin-box).</p>

<p>Une fois arrivés, les visiteurs vont venir frapper à votre <em>*box</em>
(remarquez comme ils sont polis). Cette dernière leur indiquera quelle
porte ouvrir pour accéder à ce qu’ils demandent, chaque porte donnant
accès soit à votre site web, soit à votre boîte aux lettres
électronique.</p>
</div>

Au final, c’est votre serveur qui sera là pour vous servir. Et vous
connaissez l’adage : *On n’est jamais mieux servi que par soi-même*.

Avantages de l’auto-hébergement
-------------------------------

Héberger chez soi les services que l’on utilise, ou *s’auto-héberger*
présente plusieurs avantages :

-   Les données restent chez vous. Cela veut dire que vous gardez le
    contrôle de vos fichiers. C’est particulièrement intéressant si vous
    aviez l’habitude de partager des documents à l’aide de service tiers
    (les photos chez picasa, les vidéos sur youtube, sans parler de
    mega…). Ces données restent donc chez vous et ne sont pas sur un
    lointain serveur qui peut en faire on ne sait quoi.
-   Votre vie privée est respectée. Par exemple, vos courriels ne seront
    pas scannés afin de vous proposer *gratuitement* des publicités
    correspondant à vos intérêts.
-   Vous pouvez avoir à portée de main des services qui répondent
    exactement à vos besoins.
-   Vous pouvez utiliser du matériel à faible consommation électrique et
    faire ainsi attention à la planète.
-   S’auto-héberger, c’est amusant et instructif. Cela permet de mieux
    comprendre le fonctionnement d’internet.

Inconvénients
-------------

Oui, il y a aussi quelques inconvénients :

-   Cela peut demander du temps.
-   La bande passante est limitée. Les performances seront donc
    inférieures à celle d’un serveur grand public.

<p class="alert alert-info">
Imaginez un tuyau. Lorsque vous ouvrez le robinet, vous
aurez beau le dévisser, il y a un moment où le débit d'eau
n'augmente plus. La bande passante est la quantité
maximale de données que votre connexion peut
envoyer/recevoir par seconde, ou si vous préférez, le diamètredu tuyau.
</p>

-   C’est vous qui vous chargez de la sécurité. Cela demande donc du
    soin.

[^1]: On verra ça plus tard

