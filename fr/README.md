# Documentations

## Guides

  <div class="col-xs-12">
    <a href="manueldumo/index.html" class="btn btn-lg btn-block btn-default">
      <p><b>Libertés numériques<br>
      <i>— Guide de bonnes pratiques à l'usage des DuMo</i></b></p>
    </a>
  </div>
  <div class="col-xs-12">
    <a href="auto-hebergement/index.html" class="btn btn-lg btn-block btn-default">
      <p><b>L’auto-hébergement facile<br>
      <i>— Puisqu’on n’est jamais mieux servi que par soi-même, créez l’internet que vous voulez</i></b></p>
    </a>
  </div>

## Services libres de Framasoft

<div>
  <div class="col-md-3 col-sm-6">
    <a href="zerobin/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-paste"></p>
      <p><b class="violet">Frama</b><b class="vert">bin</b></p>
      <p><b>Zerobin</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="kanboard/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-dashboard"></p>
      <p><b class="violet">Frama</b><b class="vert">board</b></p>
      <p><b>Kanboard</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="umap/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-map"></p>
      <p><b class="violet">Frama</b><b class="vert">carte</b></p>
      <p><b>uMap</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="framadate/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-calendar-check-o"></p>
      <p><b class="violet">Frama</b><b class="vert">date</b></p>
      <p aria-hidden="true"><br></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="lufi/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-send"></p>
      <p><b class="violet">Frama</b><b class="vert">drop</b></p>
      <p><b>Lufi</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="gitlab/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-git"></p>
      <p><b class="violet">Frama</b><b class="vert">git</b></p>
      <p><b>Gitlab</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="lstu/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-link"></p>
      <p><b class="violet">Frama</b><b class="vert">link</b></p>
      <p><b>Lstu</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="sympa/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-group"></p>
      <p><b class="violet">Frama</b><b class="vert">listes</b></p>
      <p><b>Sympa</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="lutim/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-photo"></p>
      <p><b class="violet">Frama</b><b class="vert">pic</b></p>
      <p><b>Lutim</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="etherpad/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-align-left"></p>
      <p><b class="violet">Frama</b><b class="vert">pad</b></p>
      <p><b>Etherpad</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="mattermost/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-comments-o"></p>
      <p><b class="violet">Frama</b><b class="vert">team</b></p>
      <b>Mattermost</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="loomio/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-bullhorn"></p>
      <p><b class="violet">Frama</b><b class="vert">vox</b></p>
      <p><b>Loomio</b></p>
    </a>
  </div>
</div>
