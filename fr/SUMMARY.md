# Summary

* [Accueil](README.md)

## Guides


* [Libertés numériques](manueldumo/README.md)
    * [1 — De quels outils ai-je besoin ?](manueldumo/chapitre-1.md)
    * [2 — Le web et les contenus](manueldumo/chapitre-2.md)
    * [3 — Mes messages sur Internet](manueldumo/chapitre-3.md)
    * [4 — Réseaux sociaux et hébergement](manueldumo/chapitre-4.md)
    * [5 — Suis-je en sécurité sur Internet ?](manueldumo/chapitre-5.md)
    * [Conclusion](manueldumo/conclusion.md)
    * [Glossaire](manueldumo/glossaire.md)

* [L'auto-hébergement facile](auto-hebergement/README.md)
    * [1 — L’auto-hébergement](auto-hebergement/intro.md)
    * [2 — Pré-requis](auto-hebergement/prerequis.md)
    * [3 — Installation de services](auto-hebergement/installation.md)
    * [4 — Services web](auto-hebergement/services_web.md)
    * [5 — Serveur de sauvegarde](auto-hebergement/sauvegarde.md)
    * [6 — Sécuriser son serveur](auto-hebergement/securite.md)
    * [7 — Les bases de données](auto-hebergement/bdd.md)
    * [8 — Divers](auto-hebergement/divers.md)
    * [Références](auto-hebergement/refs.md)


## Services libres


* [Framabin / Zerobin](zerobin/README.md)

* [Framaboard / Kanboard](kanboard/README.md)
    ### Introduction

    * [Qu'est-ce que Kanban ?](kanboard/what-is-kanban.md)
    * [Comparons Kanban aux Todo listes et à Scrum](kanboard/kanban-vs-todo-and-scrum.md)
    * [Exemples d'utilisation](kanboard/usage-examples.md)

    ### Utiliser un tableau

    * [Vues Tableau, Agenda et Liste](kanboard/project-views.md)
    * [Mode Replié et Déplié](kanboard/board-collapsed-expanded.md)
    * [Défilement horizontal et mode compact](kanboard/board-horizontal-scrolling-and-compact-view.md)
    * [Afficher ou cacher des colonnes dans le tableau](kanboard/board-show-hide-columns.md)

    ### Travailler avec les projets

    * [Types de projets](kanboard/project-types.md)
    * [Créer des projets](kanboard/creating-projects.md)
    * [Modifier des projets](kanboard/editing-projects.md)
    * [Supprimer des projets](kanboard/removing-projects.md)
    * [Partager des tableaux et des tâches](kanboard/sharing-projects.md)
    * [Actions automatiques](kanboard/automatic-actions.md)
    * [Permissions des projets](kanboard/project-permissions.md)
    * [Swimlanes](kanboard/swimlanes.md)
    * [Calendriers](kanboard/calendar.md)
    * [Analytique](kanboard/analytics.md)
    * [Diagramme de Gantt pour les tâches](kanboard/gantt-chart-tasks.md)
    * [Diagramme de Gantt pour tous les projets](kanboard/gantt-chart-projects.md)
    * [Rôles personnalisés pour les projets](kanboard/custom-project-roles.md)

    ### Travailler avec les tâches

    * [Créer des tâches](kanboard/creating-tasks.md)
    * [Fermer des tâches](kanboard/closing-tasks.md)
    * [Dupliquer et déplacer des tâches](kanboard/duplicate-move-tasks.md)
    * [Ajouter des captures d'écran](kanboard/screenshots.md)
    * [Liens internes entre les tâches](kanboard/task-links.md)
    * [Transitions](kanboard/transitions.md)
    * [Suivi du temps](kanboard/time-tracking.md)
    * [Tâches récurrentes](kanboard/recurring-tasks.md)
    * [Créer des tâches par email](kanboard/create-tasks-by-email.md)
    * [Sous-tâches](kanboard/subtasks.md)
    * [Analytique des tâches](kanboard/analytics-tasks.md)
    * [Mentionner les utilisateurs](kanboard/user-mentions.md)

    ### Travailler avec les utilisateurs

    * [Rôles](kanboard/roles.md)
    * [Gestion des utilisateurs](kanboard/user-management.md)
    * [Notifications](kanboard/notifications.md)
    * [Authentification à deux facteurs](kanboard/2fa.md)

    ### Paramètres

    * [Raccourcis clavier](kanboard/keyboard-shortcuts.md)
    * [Paramètres de l'application](kanboard/application-configuration.md)
    * [Paramètres du projet](kanboard/project-configuration.md)
    * [Paramètres du tableau](kanboard/board-configuration.md)
    * [Paramètres du calendrier](kanboard/calendar-configuration.md)
    * [Paramètres du lien](kanboard/link-labels.md)
    * [Taux de change](kanboard/currency-rate.md)

* [Framacarte / uMap](umap/README.md)
    ### Niveau débutant

    - [1 - Je consulte une carte umap](umap/1-consulter.md)
    - [2 - Je crée ma première carte umap](umap/2-premiere-carte.md)
    - [3 - J'utilise un compte et crée une belle carte](umap/3-utiliser-un-compte.md)
    - [4 - Je modifie et personnalise ma carte](umap/4-personnaliser.md)

    ### Niveau intermédiaire

    - [5 - Je crée des infobulles multimédia](umap/5-infobulles-multimedia.md)
    - [6 - Je structure ma carte avec des calques](umap/6-calques.md)
    - [7 - Je publie ma carte et en contrôle l'accès](umap/7-publication-et-droit-d-acces.md)
    - [8 - Le cas des polygones](umap/8-polygones.md)

    ### Niveau avancé

    - [9 - Je crée une carte à partir d'un tableur](umap/9-a-partir-d-un-tableur.md)
    - [11 - Je valorise les données OpenStreetMap avec uMap](umap/11-valoriser-OpenStreetMap.md)

* [Framadate](framadate/README.md)
    *   [Prise en main](framadate/prise-en-main.md)

* [Framadrop / Lufi](lufi/README.md)

* [Framagit / Gitlab](gitlab/README.md)
    *   [Comment utiliser les Gitlab Pages ?](gitlab/gitlab-pages.md)

* [Framalink / Lstu](lstu/README.md)

* [Framalistes / Sympa](sympa/README.md)

* [Framapic / Lutim](lutim/README.md)

* [Framapad / Etherpad](etherpad/README.md)
    *   [Extensions](etherpad/extensions.md)

* [Framateam / Mattermost](mattermost/README.md)
  ### Premiers pas

    *   [Identification](mattermost/help/getting-started/signing-in.md)
    *   [Bases de la messagerie](mattermost/help/getting-started/messaging-basics.md)
    *   [Configuration des notifications](mattermost/help/getting-started/configuring-notifications.md)
    *   [Organisation des conversations](mattermost/help/getting-started/organizing-conversations.md)
    *   [Recherche](mattermost/help/getting-started/searching.md)
    *   [Création d’équipes](mattermost/help/getting-started/creating-teams.md)
    *   [Gestion des membres](mattermost/help/getting-started/managing-members.md)

  ### Messagerie

    *   [Envoi de messages](mattermost/help/messaging/sending-messages.md)
    *   [Mentionner des correspondants/amis](mattermost/help/messaging/mentioning-teammates.md)
    *   [Formater le texte](mattermost/help/messaging/formatting-text.md)
    *   [Joindre des fichiers](mattermost/help/messaging/attaching-files.md)
    *   [Exécuter des commandes](mattermost/help/messaging/executing-commands.md)

  ### Paramètres

    *   [Paramètres du compte](mattermost/help/settings/account-settings.md)
    *   [Couleurs du thème](mattermost/help/settings/theme-colors.md)
    *   [Paramètres du canal](mattermost/help/settings/channel-settings.md)
    *   [Paramètres d’équipe](mattermost/help/settings/team-settings.md)

* [Framavox / Loomio](loomio/README.md)
    * [Avant de commencer](loomio/getting_started.md)
    * [Réglages de groupe](loomio/group_settings.md)
    * [Coordonner votre groupe](loomio/coordinating_your_group.md)
    * [Inviter de nouveaux membres](loomio/inviting_new_members.md)
    * [Discussion](loomio/discussion_threads.md)
    * [Commentaires](loomio/comments.md)
    * [Propositions](loomio/proposals.md)
    * [Sous-groupes](loomio/subgroups.md)
    * [Naviguer dans Loomio](loomio/reading_loomio.md)
    * [Rester à jour](loomio/keeping_up_to_date.md)
    * [Votre profil](loomio/your_user_profile.md)
