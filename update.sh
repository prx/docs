#!/bin/sh

SCRIPT=$(readlink -f $0)
SCRIPTPATH=`dirname $SCRIPT`

cd $SCRIPTPATH && git pull && cd fr/manueldumo && git pull && cd ../../ && gitbook build && chown -R $(stat -c '%U:%G' .) .
